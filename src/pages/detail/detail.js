var app = getApp()
var https = require('../../utils/https.js')
var util = require('../../utils/util.js')


Page({
    /**
     * 页面的初始数据
     */
    data: {
        id: 0,
        visible: false,
        scrolly: true,
        item: {},
        showCatelog: false,
        catelogs: {},
        recList: {},
        inBookshelf: false,
        showMoreDesc: false,
        showMoreText: '更多介绍',
        windowHeight: 0,//获取屏幕高度
        title: '',
    },
    initWindowHeight: function () {
        //获取屏幕高度
        var that = this;
        wx.getSystemInfo({
            success: function (res) {
                that.setData({
                    windowHeight: res.windowHeight
                })
            }
        })
    },
    showDesc: function (e) {
        this.setData({
            showMoreDesc: !this.data.showMoreDesc,
            showMoreText: this.data.showMoreDesc ? '更多介绍' : '收起介绍'
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (option) {
        wx.setNavigationBarTitle({
            title: option.title
        });
        var that = this;
        that.setData({
            title: option.title
        });
        that.initWindowHeight();
        var inBookshelf = false;
        if (wx.getStorageSync("book" + option.id)) {
            inBookshelf = true;
        }
        showCatelog: (option.showCatelog == "true" ? true : false)
        let url = app.globalData.detailUrl + `?id=${option.id}`;
        wx.showLoading({
            title: '加载中',
            mark: true
        });
      https.get(`https://www.zinglizingli.xyz/api/book/${option.id}.html`, function (res) {
            if (res) {
              res.book.updateTime = res.book.updateTime.substring(0,19).replace("T"," ");
            }
            that.setData({
                item: res,
                visible: true,
                id: option.id,
                inBookshelf: inBookshelf
            });
            wx.hideLoading();
        }, function () {
            wx.hideLoading();
        })
        url = app.globalData.guessUrl + `?id=1&keyword=${option.title}`
      https.get(`https://www.zinglizingli.xyz/api/book/search?catId=${option.category}&limit=8`, function (res) {
            that.setData({
                recList: res.books,
                visible: true,
            })
        })
    },
    refreshCatelog: function () {
        var that = this;
        that.initCatelog(that.data.id);
    },
    sortCatelog: function () {
        var that = this;
        var catelogs = that.data.catelogs;
        that.setData({
            catelogs: catelogs.reverse()
        });
    },
    initCatelog: function (nid) {
        var that = this;
        let url = app.globalData.catalogUrl + `?nid=${nid}`
        wx.showLoading({
            title: '加载中',
            mark: true
        });
      https.get(`https://www.zinglizingli.xyz/api/book/${nid}/index.html`, function (res) {
            that.setData({
              catelogs: res.indexList
            });
            wx.hideLoading();
        }, function () {
            wx.hideLoading();
        });
    },
    //点击事件处理
    clickDetail: function (e) {
        wx.redirectTo({
            url: `./detail?id=${e.currentTarget.dataset.id}&category=${e.currentTarget.dataset.category}&title=${e.currentTarget.dataset.title}`
        });
    },
    changeShowCatelog: function (e) {
        var that = this;
        that.setData({
            showCatelog: !that.data.showCatelog,
            scrolly: that.data.showCatelog
        })
        if (that.data.showCatelog) {
            that.initCatelog(that.data.id);
        }
    },
    toReader: function (e) {
        var that = this;
        let serialNumber = 0;
        if (e.currentTarget.dataset.serialnumber) {
            serialNumber = e.currentTarget.dataset.serialnumber;
        }
        wx.navigateTo({
          url: `../reader/reader?id=${that.data.id}&serialNumber=${serialNumber + 1}&title=${that.data.item.book.bookName}`
        });
    },
    insertBookShelf: function () {
        var that = this;
        var item = that.data.item;
        var info = new BookShelfInfo(that.data.id, item.name,
            item.image, 1, "", item.categoryname, item.lastid, item.lastitemname, util.formatDate(new Date()));

        wx.setStorage({
            key: "book" + that.data.id,
            data: info,
            success: function () {
                wx.showToast({
                    title: '已加入书架',
                })
                that.setData({
                    inBookshelf: true
                })
            }
        });
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {
        var that = this;
        return {
            title: `${that.data.title}`,
            path: `pages/detail/detail?id=${that.data.id}&title=${that.data.title}`,
            imageUrl: '',
            success: function (shareTickets) {
                console.info(shareTickets + '成功');
                // 转发成功
            },
            fail: function (res) {
                console.log(res + '失败');
                // 转发失败
            },
            complete: function (res) {
                // 不管成功失败都会执行
            }
        }
    }
})